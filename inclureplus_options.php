<?php

define ('CODE_RECUPERER_FOND', 'recuperer_fond_etendu(%s, %s, array(%s), %s)');

/**
 * Calcule un squelette avec un contexte et retourne son contenu
 * en tenant compte des arguments étendus : _filtre, _args, _sinon, _avant, _apres
 *
 * La fonction de base de SPIP : un squelette + un contexte => une page.
 * $fond peut etre un nom de squelette, ou une liste de squelette au format array.
 * Dans ce dernier cas, les squelettes sont tous evalues et mis bout a bout
 * $options permet de selectionner les options suivantes :
 *
 * - trim => true (valeur par defaut) permet de ne rien renvoyer si le fond ne produit que des espaces ;
 * - raw  => true permet de recuperer la strucure $page complete avec entetes et invalideurs
 *          pour chaque $fond fourni.
 *
 * @api
 * @param string /array $fond
 *     - Le ou les squelettes à utiliser, sans l'extension, {@example prive/liste/auteurs}
 *     - Le fichier sera retrouvé dans la liste des chemins connus de SPIP (squelettes, plugins, spip)
 * @param array $contexte
 *     - Informations de contexte envoyées au squelette, {@example array('id_rubrique' => 8)}
 *     - La langue est transmise automatiquement (sauf option étoile).
 * @param array $options
 *     Options complémentaires :
 *
 *     - trim   : applique un trim sur le résultat (true par défaut)
 *     - raw    : retourne un tableau d'information sur le squelette (false par défaut)
 *     - etoile : ne pas transmettre la langue au contexte automatiquement (false par défaut),
 *                équivalent de INCLURE*
 *     - ajax   : gere les liens internes du squelette en ajax (équivalent du paramètre {ajax})
 * @param string $connect
 *     Non du connecteur de bdd a utiliser
 * @return string|array
 *     - Contenu du squelette calculé
 *     - ou tableau d'information sur le squelette.
 */
function recuperer_fond_etendu($fond, $contexte = [], $options = [], $connect = '') {
	if (!function_exists('evaluer_fond')) {
		include_spip('public/assembler');
	}
	// assurer la compat avec l'ancienne syntaxe
	// (trim etait le 3eme argument, par defaut a true)
	if (!is_array($options)) {
		$options = ['trim' => $options];
	}
	if (!isset($options['trim'])) {
		$options['trim'] = true;
	}

	$_filtre = $_avant = $_apres = $_args = $_sinon = '';
	foreach (['connect', '_filtre', '_avant', '_apres', '_args', '_sinon'] as $k) {
		if (isset($contexte[$k])) {
			${$k} = $contexte[$k];
			unset($contexte[$k]);
		}
	}

	if (debug_get_mode('inclureplus')) {
		echo "<pre>PUIS recuperer_fond_etendu(\n $fond,\n " . var_export($contexte, true) . ",\n " . var_export($options, true) . ",\n $connect)\n_filtre=$_filtre, _avant=$_avant, _apres=$_apres, _sinon=$_sinon, _args=".var_export($_args,1)."</pre>";
	}
	$texte = '';
	$pages = [];
	$lang_select = '';
	if (!isset($options['etoile']) or !$options['etoile']) {
		// Si on a inclus sans fixer le critere de lang, on prend la langue courante
		if (!isset($contexte['lang'])) {
			$contexte['lang'] = $GLOBALS['spip_lang'];
		}

		if ($contexte['lang'] != $GLOBALS['meta']['langue_site']) {
			$lang_select = lang_select($contexte['lang']);
		}
	}

	if (!isset($GLOBALS['_INC_PUBLIC'])) {
		$GLOBALS['_INC_PUBLIC'] = 0;
	}

	$GLOBALS['_INC_PUBLIC']++;

	// fix #4235
	$cache_utilise_session_appelant	= (isset($GLOBALS['cache_utilise_session']) ? $GLOBALS['cache_utilise_session'] : null);

	foreach (is_array($fond) ? $fond : [$fond] as $f) {
		unset($GLOBALS['cache_utilise_session']);	// fix #4235

		$page = evaluer_fond($f, $contexte, $connect);
		if (debug_get_mode('inclureplus')) {
			echo "<pre>Après evaluer_fond : page=".print_r($page,1)."</pre>";
		}
		if ($page === '') {
			$c = isset($options['compil']) ? $options['compil'] : '';
			$a = ['fichier' => $f];
			$erreur = _T('info_erreur_squelette2', $a); // squelette introuvable
			erreur_squelette($erreur, $c);
			// eviter des erreurs strictes ensuite sur $page['cle'] en PHP >= 5.4
			$page = ['texte' => '', 'erreur' => $erreur];
		}
		if ($_filtre) {
			if (!$_args) {
				$_args = [];
			}
			$page['texte'] = appliquer_filtre_trace($page['texte'], $_filtre, ... $_args);
			if (debug_get_mode('inclureplus')) {
				echo "<pre>Après appliquer_filtre #$_filtre# : texte={$page[texte]}</pre>";
			}
		}
		if ($_sinon and !$page['texte']) {
			$page['texte'] = $_sinon;
			if (debug_get_mode('inclureplus')) {
				echo "<pre>Après _sinon : texte={$page[texte]}</pre>";
			}
		}
		if ($_avant) {
			$page['texte'] = $_avant . $page['texte'];
			if (debug_get_mode('inclureplus')) {
				echo "<pre>Après _avant : texte={$page[texte]}</pre>";
			}
		}
		if ($_apres) {
			$page['texte'] = $page['texte'] . $_apres;
			if (debug_get_mode ('inclureplus')) {
				echo  "<pre>Après _apres : texte={$page[texte]}</pre>";
			}
		}
		
		$page = pipeline('recuperer_fond', [
			'args' => ['fond' => $f, 'contexte' => $contexte, 'options' => $options, 'connect' => $connect],
			'data' => $page
		]);
		if (isset($options['ajax']) and $options['ajax']) {
			if (!function_exists('encoder_contexte_ajax')) {
				include_spip('inc/filtres');
			}
			$page['texte'] = encoder_contexte_ajax(
				array_merge(
					$contexte,
					['fond' => $f],
					($connect ? ['connect' => $connect] : [])
				),
				'',
				$page['texte'],
				$options['ajax']
			);
		}

		if (isset($options['raw']) and $options['raw']) {
			$pages[] = $page;
		} else {
			$texte .= $options['trim'] ? rtrim($page['texte']) : $page['texte'];
		}

		// contamination de la session appelante, pour les inclusions statiques
		if (isset($page['invalideurs']['session'])) {
			$cache_utilise_session_appelant = $page['invalideurs']['session'];
		}
	}

	// restaurer le sessionnement du contexte appelant,
	// éventuellement contaminé si on vient de récupérer une inclusion statique sessionnée
	if (isset($cache_utilise_session_appelant)) {
		$GLOBALS['cache_utilise_session'] = $cache_utilise_session_appelant;
	}

	$GLOBALS['_INC_PUBLIC']--;

	if ($lang_select) {
		lang_select();
	}
	if (isset($options['raw']) and $options['raw']) {
		return is_array($fond) ? $pages : reset($pages);
	} else {
		return $options['trim'] ? ltrim($texte) : $texte;
	}
}

// fonctions ici pour les tests
if (!function_exists('debug_get_mode')) {
	/**
	 * @param string $part : fonctionnalité testée
	 * @return bool : si l'argument debug est passé et égal à la fonctionnalité testée
	 * exemple : if (debug_get_mode('facteur')) echo $expediteur;
	 */
	function debug_get_mode($part = '')
	{
		return isset($_GET['debug'])
			and (!$part or ($_GET['debug'] == $part));
	}
}
if (!function_exists('str_concat')) {
	function str_concat (...$str) {
		return implode ('', $str);
	}
}

if (!function_exists ('appliquer_filtre_trace')) {
	function appliquer_filtre_trace ($texte, $filtre, ...$args) {
		if (debug_get_mode ('inclureplus')) {
			echo "Appliquer le filtre #$filtre# à #$texte# ";
			if ($args) {
				echo " avec les arguments ".print_r($args, 1);
			}
			echo "<br>";
		}
		$return = appliquer_filtre ($texte, $filtre, ...$args);
		if (debug_get_mode ('inclureplus')) {
			echo "Résultat : #$return#<br>";
		}
		return $return;
	}
}