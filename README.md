# Plugin expérimental

Fournit une réponse au besoin exprimé dans https://git.spip.net/spip/spip/-/issues/5028

Ajoute aux inclusions dynamiques comme `<INCLURE>` 

la possibilité, comme avec `#INCLURE`, 

de se voir appliquer des filtres et des parties conditionnelles avant et après.

## Exemple d'usage :

`<INCLURE{fond=inclure/inclusion,_filtre=strtolower,_avant=AVANTI !,_apres=et voilà...}>`

PR initiale : https://git.spip.net/spip/spip/-/merge_requests/5068